var map;
var layer;
var camaraSelect;
var colorGeomet = "rgb(0,0,255)";
var ipIONE = "localhost";
var ipRIAE = "localhost";
var accessTokenAPI = "";


define([
  "luciad/view/WebGLMap",
  "dojo/aspect",
  "dojo/request",
  "dojo/dom-construct",
  "dojo/dom",
  "luciad/model/tileset/BingMapsTileSetModel",
  "luciad/reference/ReferenceProvider",
  "luciad/view/PaintRepresentation",
  "luciad/shape/ShapeFactory",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/model/tileset/FusionTileSetModel",  
  "luciad/view/LayerType",
  "luciad/model/feature/FeatureModel",
  "luciad/model/feature/Feature",
  "luciad/model/store/MemoryStore",
  "luciad/shape/ShapeType",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/feature/BasicFeaturePainter",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/controller/BasicCreateController",
  "luciad/view/controller/EditController",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/model/store/UrlStore",
  "luciad/util/ColorMap",
  "luciad/shape/format/LonLatPointFormat",
  "samplecommon/layertreecontrol/FoldableLayerControl"
  
  
], function(WebGLMap, dAspect, request, dConstruct, dom, BingMapsTileSetModel, ReferenceProvider, PaintRepresentation, ShapeFactory, RasterTileSetLayer, FusionTileSetModel, LayerType,
			FeatureModel, Feature, MemoryStore, ShapeType, FeaturePainter, BasicFeaturePainter, FeatureLayer, BasicCreateController, EditController, GeoJsonCodec, UrlStore, ColorMap, LonLatPointFormat, FoldableLayerControl) {

	
	var mapReference = ReferenceProvider.getReference( "EPSG:4978" );
	var jsonsword;
	//conn = {}, window.WebSocket = window.WebSocket || window.MozWebSocket;
	map = new WebGLMap( "map", {reference: mapReference} );
	
	var tileSetReference = ReferenceProvider.getReference("EPSG:4326");
	var elevationParameters = {
		reference: tileSetReference,
		level0Columns: 6,
		level0Rows: 3,
		bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
		url: "http://"+ipRIAE+":8081/LuciadFusion/lts",
		coverageId: "earth_image",
		tileWidth: 255,	
		tileHeight: 255
    };
	//map.layerTree.addChild(new RasterTileSetLayer(new FusionTileSetModel(elevationParameters), { label: "Satellite imagery", layerType: LayerType.BASE }), "bottom");
	
	var elevationParameters2 = {
      reference: tileSetReference,
      level0Columns: 1,
      level0Rows: 1,
      bounds: ShapeFactory.createBounds(tileSetReference, [-85.00041654560482, 20.000833696549137, -5.000416085531469, 20.00083297011753]),
      url: "http://"+ipRIAE+":8081/LuciadFusion/lts",
      coverageId: "ElevacionSRTMCAM",
      tileWidth: 64,
      tileHeight: 64,
      dataType: FusionTileSetModel.DataType.ELEVATION,
      samplingMode: FusionTileSetModel.SamplingMode.AREA
    };
	var elavacionLayer = new RasterTileSetLayer(new FusionTileSetModel(elevationParameters2), {label: "Elevation"});
	map.layerTree.addChild(elavacionLayer);
	
	var waitingForMetadata = false;
	var switchToBing = function(type) {
		if (waitingForMetadata) {
		  return;
		}
		if (layer) {
		  map.layerTree.removeChild(layer);
		}
		waitingForMetadata = true;
		//#snippet createModel
		request("/bingproxy/" + type, {
		  handleAs: "json"
		}).then(
			function(data) {
			  //Inspect the response
			  var resourceSet;
			  var resource = data;

			  if (Object.prototype.toString.call(data.resourceSets) === '[object Array]' &&
				  data.resourceSets.length > 0) {
				resourceSet = data.resourceSets[0];
				if (Object.prototype.toString.call(resourceSet.resources) === '[object Array]' &&
					resourceSet.resources.length > 0) {
				  resource = resourceSet.resources[0];
				}
			  }

			  resource.brandLogoUri = resource.brandLogoUri || data.brandLogoUri;
			  var model = new BingMapsTileSetModel(resource);
			  layer = new RasterTileSetLayer(model, {
				layerType: LayerType.BASE
			  });
			  map.layerTree.addChild(layer, "bottom");
			  waitingForMetadata = false;
			},
			function(error) {
			  sample.warn("BingImagery", "Bing Maps is not reachable.<br/>In some cases, clearing your browser cache can help.");
			  console.log(error);
			  waitingForMetadata = false;
			}
		);
	};
	switchToBing("aerial");
	
	function openConnection() {
		//broker_conn = "ws://192.168.30.127:85";
		broker_conn = "ws://"+ipIONE+":85";
		if (conn.readyState === undefined || conn.readyState > 1) {
			conn = new WebSocket(broker_conn);
			conn.onopen = function () {
				isWSConnected = true;
				userLogin =  $("#usuarioLogin").val();
				passLogin =  $("#passwordLogin").val();
				conn.send('{"ubicacion":[{"tipo":"7","usuario":"'+userLogin+'","password":"'+passLogin+'"}]}');
			};
			conn.onmessage = function (event) {
				lmiboxdata = event.data;
				UbicarMapafuncion();
			};
			conn.onerror = function (event) {
			isWSConnected = false;
				alert("Error de Conexin\u00F3n");
			};
			conn.onclose = function (event) {
			isWSConnected = false;
				alert("Conexin\u00F3n Cerrada");
			};
		}
	}
	
	
	
	var featureReference = ReferenceProvider.getReference ("CRS:84");
	
	var featureStoreCamaras = new MemoryStore();
	var featureModelCamaras= new FeatureModel(featureStoreCamaras, {reference: featureReference});
	
	var featureStoreTroles = new MemoryStore();
	var featureModelTroles= new FeatureModel(featureStoreTroles, {reference: featureReference});
	
	function UbicarMapafuncion() {
		jsonsword = JSON.parse(lmiboxdata);
		if(jsonsword.Tipo || jsonsword.Tipo == 0)
		{
			switch (jsonsword.Tipo)
			{
				case 17:
					getMensajeTroles();
					break;
				case 18:
					getMensajeTrolesAct();
					break;
				case 22:
					getMensajeTrolesCamaras();
					break;
				case 8:
					getIncidentesT();
					break;
				default: 
					break;
			}
		}
		if(jsonsword.tipo || jsonsword.tipo == 0)
		{
			switch (jsonsword.tipo)
			{
				case "22":
					getMensajeTrolesCamaras();
					break;
				default: 
					break;
			}
		}
		if(jsonsword.tendencias)
		{
			getMensajeTendencias();
		}
		if(jsonsword.busquedas)
		{
			getMensajeBusquedas();
		}
		if(jsonsword.menciones)
		{
			getMensajeMenciones();
		}
	};
	
	
	var featureStoreIncidentesT = new MemoryStore();
	var featureModelIncidentesT = new FeatureModel(featureStoreIncidentesT, {reference: featureReference});
	
	
	//getIncidentesT();
	
	
	function getIncidentesT()
	{
		var arregloIncidentes = jsonsword.MsgInc;
		for (var i = 0; i < arregloIncidentes.length; i++)
		{
			var id_sos = arregloIncidentes[i].Id;
			var descripcion = arregloIncidentes[i].Descrip;
			var fechahora = arregloIncidentes[i].Fecha;
			var fechahoraincidente = arregloIncidentes[i].Fecha;
			var incidente = arregloIncidentes[i].Descrip;
			var estado = arregloIncidentes[i].Estado;
			var tipoestado = arregloIncidentes[i].Tipo;
			var latitud = arregloIncidentes[i].Lat;
			var longitud = arregloIncidentes[i].Lon;
			var simbolo = arregloIncidentes[i].Simbolo;
			
			if(simbolo == "warninga.png")
				simbolo = "SOSMedio.png";
			if(simbolo == "warningv.png")
				simbolo = "SOSBajo.png";
			if(simbolo == "warningr.png")
				simbolo = "SOSAlto.png";
			
			var point_SOS_consulta = ShapeFactory.createPoint(featureReference, [longitud, latitud, 0]);
			
			
			var item5 = document.createElement('div');
			var img5 = document.createElement('img');
			img5.src = 'img/' + simbolo;
			img5.style.width = '13px';
			img5.style.height = '13px';
			item5.appendChild(img5);
			
			var fechaDescCamb = fechahora.split(" ")[0];
			var horaDescCamb = fechahora.split(" ")[1];
			
			
			var itemNew = {id: (id_sos+2000), content: item5, start: new Date(fechaDescCamb.split("/")[2],fechaDescCamb.split("/")[1] - 1,fechaDescCamb.split("/")[0],horaDescCamb.split(":")[0],horaDescCamb.split(":")[1],0,0)}
			
			
			var featureTemp_actas_consulta = featureModelIncidentesT.get(id_sos);
			var actas_consulta = new Feature(point_SOS_consulta, {
				id_sos: id_sos,
				descripcion: descripcion,
				fechahora: fechahora,
				fechahoraincidente: fechahoraincidente,
				incidente: incidente,
				estado: estado,
				tipoestado: tipoestado,
				latitud: latitud,
				longitud: longitud,
				simbolo: simbolo
			},id_sos);
			
			if(featureTemp_actas_consulta)
			{
				if(featureTemp_actas_consulta.shape[0] != actas_consulta.shape[0] || featureTemp_actas_consulta.shape[1] != actas_consulta.shape[1])
				{
					featureModelIncidentesT.put(actas_consulta);
				}
			}
			else
			{
				featureModelIncidentesT.add(actas_consulta);
				items._addItem(itemNew);
			}
		}
		timeline.setItems(items);
	}
	
	function MensajeBalloonIncidentesT() {
	  return function(o) {
		  featInfoEnvi = o;
		  return "<div id='objetoswordpane' class='formpanel' style='padding:10px; overflow-y: scroll; height: 250px; width: 100%; margin-bottom :5px;'>"+
				"<table  class='table' width='250'><tbody>"+
				
				"<tr>"+
				"<td>"+
				"<label>Descripción: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["descripcion"] + "</label>"+
				"</td>"+
				"</tr>"+
				"<tr>"+
				"<td>"+
				"<label>Fecha: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["fechahora"] + "</label>"+
				"</td>"+
				"</tr>"+
				"<tr>"+
				"<td>"+
				"<label>Incidente: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["incidente"] + "</label>"+
				"</td>"+
				"</tr>"+
				"<tr>"+
				"<td>"+
				"<label>Estado: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["tipoestado"] + "</label>"+
				"</td>"+
				"</tr>"+

				
				
				"</tbody></table>"+
				"</div>";
	  };
	};
	
	var actasPainterConsulta = new FeaturePainter();
	actasPainterConsulta.paintBody = function (geocanvas, feature, shape, layer, map, paintState)
	{
		geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/" + feature.properties["simbolo"], offsetX: 0,offsetY: 0});
		if (paintState.selected ) {
			geocanvas.drawIcon(shape, {width: 10, height: 10, url: "img/visto.png"} );
		}
	}
	
	var IncidentesLayer = new FeatureLayer(featureModelIncidentesT, {
		selectable: true,
		editable: true,
		label: "Incidentes Consultados",
		painter: actasPainterConsulta
	});
	
	map.layerTree.addChild(IncidentesLayer, "top");
	IncidentesLayer.balloonContentProvider = MensajeBalloonIncidentesT();
	
	var busquedasTroles = "<option>Todos</option>";
	
	function getMensajeTendencias()
	{
		for (var i=0;i<jsonsword.tendencias.trends.length;i++)
		{
			
		}
		
	}
	
	
	function getMensajeBusquedas()
	{
		for (var i=0;i<jsonsword.busquedas.length;i++)
		{
			
		}	
	}
	
	function getMensajeMenciones()
	{
		for (var i=0;i<jsonsword.menciones.length;i++)
		{
			
		}	
	}
	
	var busqUnidad = "";
	var arreglobusqUnidad = new Array();
	var busqDireccion = "";
	var arreglobusqDireccion = new Array();
	
	
	var busqCamaras = "";
	var arreglobusqCamaras = new Array();
	
	var busqParadas = "";
	var arreglobusqParadas = new Array();
	
	function getMensajeTrolesCamaras()
	{
		for (var i=0;i<jsonsword.camaras.length;i++)
		{
			var id_camara = jsonsword.camaras[i].id_camara;
			var ip = jsonsword.camaras[i].ip;
			var latitud = jsonsword.camaras[i].latitud;
			var longitud = jsonsword.camaras[i].longitud;
			var passwordC = jsonsword.camaras[i].password;
			var usuario = jsonsword.camaras[i].usuario;
			var ubicacion = jsonsword.camaras[i].ubicacion;
			var pointCamara = ShapeFactory.createPoint(featureReference, [longitud, latitud, 0]);
			var featureTempCamara = featureModelCamaras.get(id_camara);
			var camaraN = new Feature(pointCamara, {
					id_camara: id_camara,
					ip: ip,
					passwordC: passwordC,
					usuario: usuario,
					ubicacion: ubicacion
					},id_camara);
			
		
			if(featureTempCamara)
			{
				if(featureTempCamara.shape[0] != camaraN.shape[0] || featureTempCamara.shape[1] != camaraN.shape[1])
				{
					featureModelCamaras.put(camaraN);
				}
			}
			else
			{
				featureModelCamaras.add(camaraN);
			}
			
			if(arreglobusqCamaras.indexOf(ubicacion) == -1)
			{
				//busqCamaras += "<option>"+Placa+"</option>";
				arreglobusqCamaras.push(ubicacion);
			}
		}
		
		arreglobusqCamaras.sort();
		busqCamaras = "";
		for (var j=0;j<arreglobusqCamaras.length;j++)
		{
			busqCamaras += "<option>"+arreglobusqCamaras[j]+"</option>";
		}
		document.getElementById('camarasL').innerHTML = busqCamaras;
		
	}
	
	function getMensajeTroles()
	{
		for (var i=0;i<jsonsword.MsgTrackTrole.length;i++)
		{
			var simboloTrole = "trole.png";
			var idTrole = jsonsword.MsgTrackTrole[i].Id_trole;
			var latTrole = jsonsword.MsgTrackTrole[i].Lat;
			var lonTrole = jsonsword.MsgTrackTrole[i].Lon;
			var Placa = jsonsword.MsgTrackTrole[i].Placa.split(" - ")[0];
			Placa = Placa.split("-")[0];
			var Direccion = jsonsword.MsgTrackTrole[i].Direccion;
			var Dispocitivo = jsonsword.MsgTrackTrole[i].Dispocitivo;
			busquedasTroles += "<option>"+jsonsword.MsgTrackTrole[i].Placa+"</option>";
			var pointTrole = ShapeFactory.createPoint(featureReference, [lonTrole, latTrole, 0]);
			var featureTempTroleAP = featureModelTroles.get(idTrole);
			var sentiTrole = "Sin Noviemto";
			if(featureTempTroleAP)
			{
				if(featureTempTroleAP.shape[1] < latTrole)
				{
					sentiTrole = "S - N";
				}
				else
				{
					if(featureTempTroleAP.shape[1] > latTrole)
					{
						sentiTrole = "N - S";
					}
				
				}
			}
			
			var troleN = new Feature(pointTrole, {
					id: idTrole,
					codigo: simboloTrole,
					Placa: Placa,
					ActualizacionTV: jsonsword.MsgTrackTrole[i].ActualizacionTV,
					Telefono: jsonsword.MsgTrackTrole[i].Telefono,
					Dispocitivo: Dispocitivo,
					Direccion: Direccion,
					Velocidad: jsonsword.MsgTrackTrole[i].Velocidad,
					Nombre: jsonsword.MsgTrackTrole[i].Nombre,
					Actualizacion: jsonsword.MsgTrackTrole[i].Actualizacion,
					sentido: sentiTrole,
					buscasdo: 0
			},idTrole);
			
			
			if(arreglobusqUnidad.indexOf(Placa) == -1)
			{
				//busqUnidad += "<option>"+Placa+"</option>";
				arreglobusqUnidad.push(Placa);
			}
			
			if(arreglobusqDireccion.indexOf(Direccion) == -1)
			{
				busqDireccion += "<option>"+Direccion+"</option>";
				arreglobusqDireccion.push(Direccion);
			}
			
			if(featureTempTroleAP)
			{
				if(featureTempTroleAP.shape[0] != troleN.shape[0] || featureTempTroleAP.shape[1] != troleN.shape[1])
				{
					featureModelTroles.put(troleN);
				}
				else
				{
					
				}
			}
			else
			{
				featureModelTroles.add(troleN);
			}
		}
		arreglobusqUnidad.sort();
		busqUnidad = "";
		for (var j=0;j<arreglobusqUnidad.length;j++)
		{
			busqUnidad += "<option>"+arreglobusqUnidad[j]+"</option>";
		}
		document.getElementById('unidadL').innerHTML = busqUnidad;
		document.getElementById('direccionL').innerHTML = busqDireccion;
	}
	
	function getMensajeTrolesAct()
	{
		for (var i=0;i<jsonsword.MsgTrackTrole.length;i++)
		{
			var simboloTrole = "trole.png";
			var idTrole = jsonsword.MsgTrackTrole[i].Id_trole;
			var latTrole = jsonsword.MsgTrackTrole[i].Lat;
			var lonTrole = jsonsword.MsgTrackTrole[i].Lon;
			var pointTrole = ShapeFactory.createPoint(featureReference, [lonTrole, latTrole, 0]);
			var featureTempTrole = featureModelTroles.get(idTrole);
			var troleN = new Feature(pointTrole, {
					id: idTrole,
					codigo: simboloTrole,
					Placa: featureTempTrole.properties.Placa,
					ActualizacionTV: featureTempTrole.properties.ActualizacionTV,
					Nombre: featureTempTrole.properties.Nombre,
					Actualizacion: featureTempTrole.properties.Actualizacion
			},idTrole);
			
			if(featureTempTrole)
			{
				featureModelTroles.put(troleN);
			}
		}		
	}
	
	function MensajeBalloonTrole() {
		return function(o) {
			return "<div id='objetoswordpane' class='formpanel' style='padding:10px; width: 100%; margin-bottom :5px;'>"+
			"<table  width='200'>"+


			"<tr>"+
			"<td colspan='2' class='text-center'>"+
			"<label>Unidad " + o.properties["Placa"] +
			"</label></td>"+
			"</tr>"+
			
			"<tr>"+
			"<td>"+
			"<label>Ultima Actualizaci\u00F3n: </label>"+
			"</td>"+
			"<td >"+
			"<label>" + o.properties["Actualizacion"] + "</label>"+
			"</td>"+
			"</tr>"+

			"<tr>"+
			"<td>"+
			"<label>Direcci\u00F3n: </label>"+
			"</td>"+
			"<td >"+
			"<label>" + o.properties["Direccion"] + "</label>"+
			"</td>"+
			"</tr>"+


			"<tr>"+
			"<td>"+
			"<label>Velocidad: </label>"+
			"</td>"+
			"<td >"+
			"<label>" + o.properties["Velocidad"] + "</label>"+
			"</td>"+
			"</tr>"+

			"<tr>"+
			"<td>"+
			"<label>Dispositivo: </label>"+
			"</td>"+
			"<td >"+
			"<label>" + o.properties["Dispocitivo"] + "</label>"+
			"</td>"+
			"</tr>"+

			"<tr>"+
			"<td>"+
			"<label>Info: </label>"+
			"</td>"+
			"<td >"+
			"<label>" + o.properties["sentido"] + "</label>"+
			"</td>"+
			"</tr>"+
			

			"</table>"+
			"</div>";

		};
	};

	var trolePainter = new FeaturePainter();
	var troleLayer;
	trolePainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
	{
		geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/" + feature.properties.codigo, offsetX: 0,offsetY: 0});
		var mumeroUnidad = parseInt(feature.properties["Placa"]).toString();
		var digtSig = 0;
		for (var iCont=0;iCont<mumeroUnidad.length;iCont++)
		{
			var iconoNum = "img/num/" + mumeroUnidad.charAt(iCont) + ".png";
			geocanvas.drawIcon( shape, {width: 20, height: 20, url: iconoNum, offsetX: 10 + digtSig,offsetY: 10});
			digtSig = digtSig + 10;
		}
		if(feature.properties["buscasdo"] == 1)
		{
			geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/visto.png", offsetX: 10,offsetY: -10});
		}
		if (paintState.selected ) {
			geocanvas.drawIcon(shape, {width: 10, height: 10, url: "img/visto.png", offsetX: 10,offsetY: -10} );
		}
		 //var label = "<span style='color: #FFFFFF'>" + feature.properties.Placa + "</span>";
		 //labelcanvas.drawLabel(label, shape.focusPoint, {});
		//geocanvas.drawLabel( feature.properties["Placa"], shape, {offsetX: 16 , offsetY: 16});
	}
	
	
	troleLayer = new FeatureLayer(featureModelTroles, {
		selectable: true,
		editable: true,
		label: "Trole",
		painter: trolePainter
	});
	
	troleLayer.balloonContentProvider = MensajeBalloonTrole();
	map.layerTree.addChild(troleLayer, "top");
		
	$("#ocultarTimeLine").click(function(){
		$('#timeline').hide();
		$("#map").css("height", "100%");
		$("#map").resize();
	});
		
	$("#BingCalles").click(function(){
		switchToBing("road");
	});
	
	$("#BingSatelital").click(function(){
		switchToBing("aerial");
	});
			
	var featureStoreParadasTrole = new MemoryStore();
	var featureModelParadasTrole = new FeatureModel(featureStoreParadasTrole, {reference: featureReference});
		
	var featureParadasTroleBusPainter = new FeaturePainter();
	featureParadasTroleBusPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
	{
		geocanvas.drawIcon( shape, {width: 15, height: 15, url: "img/paradatrole2.png" , offsetX: 0,offsetY: 0});
		if (paintState.selected ) {
			geocanvas.drawIcon(shape, {width: 10, height: 10, url: "img/visto.png", offsetX: 10,offsetY: -10} );
		}
	}
     
	
	var featurefeatureModelTroles = new FeatureLayer(featureModelParadasTrole, {
		  selectable: true,
		  editable: true,
		  label: "Paradas Trolebus",
		  painter: featureParadasTroleBusPainter
	});
		
	function MensajeBalloonParadasTroleBus(){
	  return function(o) {
            return "<div id='objetoswordpane' class='formpanel' style='padding:10px'>"+
				"<table  class='table' width='250'>"+
				
				"<tbody><tr>"+
				"<td>"+
				"<label>Parada: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Name"] + "</label>"+
				"</td>"+
				
				
				"</tr></tbody>"+	
				
				"</table> "+
				"</div>";
          };

	};
	
	featurefeatureModelTroles.balloonContentProvider = MensajeBalloonParadasTroleBus();
	map.layerTree.addChild(featurefeatureModelTroles);	
	
	function MensajeBalloonCamaras() {
		 return function(o) {
			 camaraSelect = o;
		return "<div id='objetoswordpane' class='formpanel' style='padding:10px'><div class='row'><div class='text-center'>"+
			"<h2>C\u00E1mara "+o.properties["ubicacion"]+"</h2><iframe name='window' src='http://"+o.properties["usuario"]+":"+o.properties["passwordC"]+"@"+o.properties["ip"]+"/axis-cgi/mjpg/video.cgi?fps=12&resolution=320x240' width='320' height='240' marginwidth='0' scrolling='yes' frameborder='0'></iframe>"+
			"</div></div>"+
			"<div class='row'><div class='text-center'><p><button type='button' class='btn btn-default btn-xs' onclick='abrirModal()'>Extender</button><button type='button' class='btn btn-default btn-xs'>Calibrar Camara</button></p></div></div>"+
			"</div>";
	  };
	};

	var CamarasPainter = new FeaturePainter();
	var CamarasLayer;
	CamarasPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
	{
		geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/camera.png", offsetX: 0,offsetY: 0});
		if (paintState.selected ) {
			geocanvas.drawIcon(shape, {width: 10, height: 10, url: "img/visto.png", offsetX: 10,offsetY: -10} );
		}
	}
	
	CamarasLayer = new FeatureLayer(featureModelCamaras, {
		selectable: true,
		editable: true,
		label: "Camaras",
		painter: CamarasPainter
	});
	
	CamarasLayer.balloonContentProvider = MensajeBalloonCamaras();
	map.layerTree.addChild(CamarasLayer, "top");
	
	var drawingPainter = new FeaturePainter();
	drawingPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
	{
		geocanvas.drawShape(shape, { stroke: { color: colorGeomet, width: 3 } });
		if (paintState.selected ) {
			geocanvas.drawShape(shape, { stroke: { color: "rgb(255,255,255)", width: 3 } });
		}
	}
	
	var geometryStoreDrawing = new MemoryStore();
        var geometryModelDrawing = new FeatureModel(geometryStoreDrawing, {reference: featureReference});
        var geometryLayer = new FeatureLayer (geometryModelDrawing,
        {
        	selectable: true,
            editable: true,
            label: "Geometría",
			painter: drawingPainter
        });
	map.layerTree.addChild(geometryLayer);
		
	map.mapNavigator.fit(ShapeFactory.createBounds(featureReference,
					[-78.517248, 0.15394240, -0.384742, 0.35291812]), false);
		
	var mapLayer = [];
	var idA = [];
  
	(function () {
		"use strict";
		var $list = $("#menuwrapper .nav .nav-second-level");
		var tmplt = "<li > <a id={{id}}a href='#' class = 'menustyle'><i class='{{image}}' aria-hidden='true' ></i> {{name}} <span id = {{idhide}}hide class='glyphicon glyphicon-eye-open pull-right'></span></a> </li>"
		$list.on( "updatedList", updateList );
		function updateList ( event, names ) {
		$list.html($.map( names, function ( name ) {
			var nameCompleto = name.split('|');
			idA.push(nameCompleto[0].replace(' ','').toLowerCase() + 'a');
			mapLayer.push({'key':nameCompleto[0].replace(' ','').toLowerCase() + 'hide', 'value':buscarLayer(nameCompleto[0])});
			return  tmplt.replace( /{{id}}/, nameCompleto[0].replace(' ','').toLowerCase()).replace(/{{image}}/, nameCompleto[1]).replace(/{{name}}/, nameCompleto[0]).replace(/{{idhide}}/, nameCompleto[0].replace(' ','').toLowerCase());
		}).join(""));


		}
		// Anytime you want to update, pass the new names into a trigger
		$list.trigger( "updatedList", [ [ 
				"Trolebuses|fa fa-bus", 
				"Paradas Trolebus|fa fa-square-o", 
				"Twitter|fa fa-twitter",
				"Camaras|fa fa-twitter", 
				"Incidentes|fa fa-exclamation",
				"Elevaciones|fa fa-level-up", 
				"Bing maps|fa fa-map"] ] );

		}()
	);
  
    $('#hospitalesantioquiali').click(function(){
	   map.mapNavigator.fit(featurePuntosAntioquiaLayer.bounds, false);
	});
	
	$('#nuevoUsuarioMod').click(function(){
		$('#myModalNuevoUsuario').modal('show');
	});
	
	$('#agregarNuevoUsuario').click(function(){
		$('#myModalTablaFeature').modal('show');
	});
	
	function buscarLayer(name){
		var capa;
		switch(name) {
			case "Trolebuses":
				capa = troleLayer
				break;
			case "Paradas Trolebus":
				capa = featurefeatureModelTroles
				break;
			case "Bing maps":
				capa = layer;
				break;
			case "Camaras":
				capa = CamarasLayer;
				break;
			case "Incidentes":
				capa = IncidentesLayer;
				break;
			default:
				break;
		}
		return capa;
	}
	
	$(this).keypress(function(event)
	{
		var keycode = (event.keyCode ? event.keyCode : event.which);
		 //Edit Geometry (e)
		 if ( keycode == "69" || keycode == "101") {
			   if (map.selectedObjects[0]) {
				 map.controller = new EditController(map.selectedObjects[0].layer, map.selectedObjects[0].selected[0]);
			   }
		 }
		 //Delete Geometry (del or backspace)
		 if ( keycode == "46" || keycode == "8") {
			 if ( map.selectedObjects[0] && geometryLayer === map.selectedObjects[0].layer ) {
			  geometryModelDrawing.remove( map.selectedObjects[0].selected[0].id );
			 }
		}
	});
	
	$("#envioNoticiaNew").click(function(){
		var banderaGeocerca = document.getElementById("checkGeoCerca").checked;
		var banderaTodos = document.getElementById("checkTodos").checked;
		var banderaNS = document.getElementById("checkNS").checked;
		var banderaSN = document.getElementById("checkSN").checked;
		var mensajeUr = document.getElementById("urgenteMs").checked;
		var enviarMSM = false;
		
		var busesTex = $("#busesWrite").val();
		
		var mensajeAEnviar = {"id":SHA1(new Date().toString()),"enabled":true,"content":$("#messageTroleBuses").val(),"emergency":false,"devices":[]}
		
		if(mensajeUr)
		{
			mensajeAEnviar.emergency = true;
		}
		
		
		var arregloBusContenidos = new Array();
		var mensajeEnviar = {'ubicacion':[{'tipo':'11','enabled': true,'content': $("#messageTroleBuses").val(),'emergency': true,'devices': []}]};
		
		if(busesTex.includes(","))
		{
			var numerosEscr = busesTex.split(","),
				iEd;

			for (iEd = 0; iEd < numerosEscr.length; iEd++) {
				mensajeAEnviar.devices.push(parseInt(numerosEscr[iEd]).toString());
			}
			enviarMSM = true;
		}
		else if(busesTex.includes("-"))
		{
			var numerosEscr = busesTex.split("-"),
				iEd;
				
			var inicioIdBus = numerosEscr[0];
			var finIdBus = numerosEscr[1];

			for (iEd = inicioIdBus; iEd < finIdBus; iEd++) {
				mensajeAEnviar.devices.push(parseInt(iEd.toString()));
				enviarMSM = true;
			}
		}
		else if (!(busesTex == null || busesTex.length == 0))
		{
			if(parseInt(busesTex) >= 1 &&  parseInt(busesTex) <= 80)
			{	
				mensajeAEnviar.devices.push(parseInt(busesTex).toString());
				enviarMSM = true;
			}
			else
				alert("Cadena no valida");
		}
		
		if(banderaGeocerca)
		{
			var valueGeo = geometryModelDrawing.query();
			while(valueGeo.hasNext())
			{
				var geometriaF = valueGeo.next()
				var valueBus = featureModelTroles.query();
				while(valueBus.hasNext())
				{
					var busF = valueBus.next()
					if(geometriaF.shape.contains2D(busF.shape.focusPoint))
					{
						mensajeAEnviar.devices.push(parseInt(busF.properties["Placa"]).toString());
						enviarMSM = true;
					}
				}
			}
		}
		if(banderaTodos)
		{
			var jsonStringMen = JSON.stringify(mensajeEnviar);  
			//alert(jsonStringMen);
			conn.send(jsonStringMen);
		}
		if(banderaNS)
		{
			
				var valueBus = featureModelTroles.query();
				while(valueBus.hasNext())
				{
					var busF = valueBus.next()
					if(busF.properties["sentido"] == "N - S")
					{
						//alert();
						mensajeAEnviar.devices.push(parseInt(busF.properties["Placa"]).toString());
						enviarMSM = true;
					}
				}
			var jsonStringMen = JSON.stringify(mensajeEnviar);  
			//alert(jsonStringMen);
			conn.send(jsonStringMen);
		}
		if(banderaSN)
		{
			
			var valueBus = featureModelTroles.query();
			while(valueBus.hasNext())
			{
				var busF = valueBus.next()
				if(busF.properties["sentido"] == "S - N")
				{
					mensajeAEnviar.devices.push(parseInt(busF.properties["Placa"]).toString());
					enviarMSM = true;
				}
			}
			var jsonStringMen = JSON.stringify(mensajeEnviar);  
			//alert(jsonStringMen);
			conn.send(jsonStringMen);
		}
		
		if(enviarMSM)
		{
			$.ajax({
				url: 'http://200.24.218.202/api/news',
				type: 'post',
				dataType: 'application/json',
				data: mensajeAEnviar,
				headers: {
					"tvip-apikey": "a09e5fa510db37e4e0a7dc403a84900edab49775"
				},
				dataType: 'json',
				success: function (data) {
					console.info(data);
				}
			});
		}
		
		
	});
	
	$("#buscarPlacaN").click(function(){
		var placaAbusq  = $("#unidadL").val();
		var value = featureModelTroles.query();
		while(value.hasNext())
		{
			var dondeBus = value.next()
			if(placaAbusq == dondeBus.properties["Placa"])
			{
				
				dondeBus.properties["buscasdo"] = 1;
				var boundsInc = dondeBus.shape.bounds;
				boundsInc.setTo2D(boundsInc.x-0.01,0.02,boundsInc.y-0.01,0.02);
				map.mapNavigator.fit(boundsInc);
			}
			else
			{
				dondeBus.properties["buscasdo"] = 0;
			}
		}
		featurefeatureModelTroles.painter = featureParadasTroleBusPainter;
	});
	
	
	$("#buscarPorDirecc").click(function(){
		var direccionAbusq  = $("#direccionL").val().replace(/ /g, "");
		var value = featureModelTroles.query();
		while(value.hasNext())
		{
			var dondeBus = value.next()
			var etiqueta = dondeBus.properties["Direccion"].replace(/ /g, "").replace(/\n/g, "");
			if(direccionAbusq == etiqueta)
			{
				var boundsInc = dondeBus.shape.bounds;
				boundsInc.setTo2D(boundsInc.x-0.01,0.02,boundsInc.y-0.01,0.02);
				map.mapNavigator.fit(boundsInc);
				break;
			}
		}
	});
	
	
	$("#buscarCamarasN").click(function(){
		var camaraAbusq  = $("#camarasL").val();
		var value = featureModelCamaras.query();
		while(value.hasNext())
		{
			var dondeCamara = value.next()
			if(camaraAbusq == dondeCamara.properties["ubicacion"])
			{
				var boundsInc = dondeCamara.shape.bounds;
				boundsInc.setTo2D(boundsInc.x-0.01,0.02,boundsInc.y-0.01,0.02);
				map.mapNavigator.fit(boundsInc);
				break;
			}
		}
	});
	
	
	$("#buscarParadasN").click(function(){
		var camaraAbusq  = $("#paradasL").val().replace(/ /g, "");
		var value = featureModelParadasTrole.query();
		while(value.hasNext())
		{
			var dondeParada = value.next()
			if(camaraAbusq == dondeParada.properties["Name"].replace(/ /g, ""))
			{
				var boundsInc = dondeParada.shape.bounds;
				boundsInc.setTo2D(boundsInc.x-0.01,0.02,boundsInc.y-0.01,0.02);
				map.mapNavigator.fit(boundsInc);
				break;
			}
		}
	});
	
	
	
	$(document).ready(function () {
		conn = {}, window.WebSocket = window.WebSocket || window.MozWebSocket;
		$("#connectsword").click(function(){
			openConnection();
		});
		
		$('[data-toggle="popover"]').popover({ html : true }).on('shown.bs.popover', function() {
			$("#graficarArea").click(function(){
				GraficarTipo(0);
			});	
			$("#graficarCirculo").click(function(){
				GraficarTipo(1);
			});
			$("#graficarCirculo2").click(function(){
				GraficarTipo(2);
			});
			$("#graficarOvalo").click(function(){
				GraficarTipo(3);
			});
			var colorInput = document.getElementById("colorGraf");
			colorInput.addEventListener("input", function() {
				colorGeomet = colorInput.value;
			}, false); 
		});
		
		function GraficarTipo(tipoG)
		{
			var tipoAgraf = ShapeType.POLYGON;
			if(tipoG == 0)
			{
				tipoAgraf = ShapeType.POLYGON;
			}
			if(tipoG == 1)
			{
				tipoAgraf = ShapeType.CIRCLE_BY_3_POINTS;
			}
			if(tipoG == 2)
			{
				tipoAgraf = ShapeType.CIRCLE_BY_CENTER_POINT;
			}
			if(tipoG == 3)
			{
				tipoAgraf = ShapeType.ELLIPSE;
			}
			var value = geometryModelDrawing.query();
			while(value.hasNext())
			{
				geometryModelDrawing.remove(value.next().id)
			}
			var polyController = new BasicCreateController(tipoAgraf);
			polyController.setPointCount(3, -1);
			map.controller = polyController;
			
		}
		
		$.ajax({url: "data/ParametrosC2Viz.json", success: function(resultParm){
			 ipRIAE = resultParm.ipRIA;
			 ipIONE = resultParm.ipION;
		}});
		
		$.ajax({url: "data/paradasTrole.geojson", success: function(resultObt){
			var codecGson = new GeoJsonCodec({mode3D: false});
			var cursorFeaTe = codecGson.decode({content: resultObt});
			while(cursorFeaTe.hasNext())
			{
				var featTemp = cursorFeaTe.next();
				featureModelParadasTrole.add(featTemp);
				var paradasNew = featTemp.properties.Name;
				if(arreglobusqParadas.indexOf(paradasNew) == -1)
				{
					//busqCamaras += "<option>"+paradasNew+"</option>";
					arreglobusqParadas.push(paradasNew);
				}
			}
			arreglobusqParadas.sort();
			busqCamaras = "";
			for (var j=0;j<arreglobusqParadas.length;j++)
			{
				busqCamaras += "<option>"+arreglobusqParadas[j]+"</option>";
			}
			document.getElementById('paradasL').innerHTML = busqCamaras;
		}});
		
		 var ParametrosSend = {
          client_id: "r2PRsSphduzmRds0Rw731D7tuir8hRfLtWt0rjdk",
          client_secret: "MLwHuYQqCm4Is702rtAd4rTfjBT4hkPC7O5RdSKyGEuHWZQfT3yGWd7uQ8NaFhHQy3iRcEF092RZ31tx9p1NanilPJNSOtIuVmggeG2UAwuE3cUDQ7i5tbmQ2MTvbH8dfHClrUIsYapY2pPNIcuVDX0RJjy2hWvFxEf6GtBxTINQV5BW494ErGkDxItjesT6Tqh7gdLrs2NqPgsZkEp4qQPXC5SViyFtZz0xKxpOmFyXXFPjzgNihUlnvyFrmw",
          grant_type: "client_credentials"
        }
		
		
		$.ajax({data: ParametrosSend, url: "http://200.124.230.134/api/authorize/access_token", type: 'post', success: function(resultObt){
			//alert(resultObt);
			//document.getElementById('pepitall').innerHTML = resultObt;
			var credencialesAPI = codecGson.decode({content: resultObt});
			accessTokenAPI = credencialesAPI.access_token;
		}});
		
		
		//$('#tablaAtributos').DataTable();
		
	});
	
	
	
	
		
	$('#side-menu').click(function(e){
		if (e.target.tagName === 'SPAN'){
			//alert("span" + e.target.id);  
			
			var capa;
						
			for (var j = 0; j < mapLayer.length; j += 1) {
			if(mapLayer[j].key == e.target.id)
				capa = mapLayer[j].value;
			}

						
			var getClassOpen = document.getElementsByClassName("glyphicon glyphicon-eye-open pull-right");
			var bandera = 0;
			for (var i = 0; i < getClassOpen.length; i++) {
				//console.log(getClassOpen[i].className); //second console output
				//console.log(getClassOpen[i].id);
				
				if(getClassOpen[i].id == e.target.id){
					if (getClassOpen[i].className == 'glyphicon glyphicon-eye-open pull-right')
					{
						if (e.target.id == "bingmapshide")
							capa = layer;
						bandera = 1;
						$('#' + getClassOpen[i].id).removeClass('glyphicon glyphicon-eye-open pull-right').addClass('glyphicon glyphicon-eye-close pull-right');
						//featurePuntosAntioquiaLayer.visible = false;
						capa.visible = false;
						
						
						/*"Twitter", "Semaforos","GPS","Camaras","Hospitales Antioquia","Quito", "Parqueaderos", "Transito",
									"Mapa de calor","Autobuses","Calles", "Barrios", "Insidentes consultados", "Unidades realtime",
									"Elevaciones", "Bing maps"*/
									
					}
				}
			}	
			if (bandera == 0)
			{
				var getClassClose = document.getElementsByClassName("glyphicon glyphicon-eye-close pull-right");
				for (var i = 0; i < getClassClose.length; i++) {
				//console.log(getClassOpen[i].className); //second console output
							
					if(getClassClose[i].id == e.target.id){
						if (getClassClose[i].className == 'glyphicon glyphicon-eye-close pull-right')
						{
							if (e.target.id == "bingmapshide")
							capa = layer;
							bandera = 0;
							$('#' + getClassClose[i].id).removeClass('glyphicon glyphicon-eye-close pull-right').addClass('glyphicon glyphicon-eye-open pull-right');
							capa.visible = true;
						}
					}
				}
			}
		}
		else if (e.target.tagName === 'A'){
			//alert("a" + e.target.id);  
			
			var capa;
						
			for (var j = 0; j < mapLayer.length; j += 1) {
				if(mapLayer[j].key == e.target.id.substring(0,e.target.id.length-1) + 'hide'){
					capa = mapLayer[j].value;
					if (capa.bounds  === undefined){
						
					}else{
						map.mapNavigator.fit(capa.bounds, false);
					}
				}
			}
		}
	});		
 
});

	
function cambiarFocus(valorCambiado)
{
	
	$.ajax
	({
		  type: "GET",
		  dataType: 'json',
		  url: "http://169.254.200.211/axis-cgi/opticssetup.cgi?afocus="+valorCambiado/297,
		  beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", "Basic " + btoa("root:12345678"));
		  }
});
}

function abrirModal()
{
	document.getElementById('camaraZoom').src = "http://"+camaraSelect.properties["usuario"]+":"+camaraSelect.properties["passwordC"]+"@"+camaraSelect.properties["ip"]+"/axis-cgi/mjpg/video.cgi?fps=12&resolution=768x576";
	
	$('#myModalCamraa').modal('show');
}



