var randomScalingFactor = function() {
return Math.round(Math.random() * 100);
};
var randomColorFactor = function() {
	return Math.round(Math.random() * 255);
};
var randomColor = function(opacity) {
	return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
};

var dataCharts = [30,30,20];

var CanvasNew = function() {
	if(document.getElementById("chart-area"))
	{
		var config = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: dataCharts,
					backgroundColor: [
						"#FF0000",
						"#FFFF00",
						"#00FF00",
					],
					label: 'Dataset 1'
				} ],
				labels: [
					"Malo",
					"Medio",
					"Bueno"
				]
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
					labels: {
						fontColor: '#FFF'
					}
				},
				title: {
					display: true,
					text: 'Nivel Seguridad',
					fontColor: '#FFF'
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};
		
		var ctx = document.getElementById("chart-area").getContext("2d");
		window.myDoughnut = new Chart(ctx, config);
	}
};