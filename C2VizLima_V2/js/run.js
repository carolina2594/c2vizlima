require({
  baseUrl: "../../",
  packages:[
    {name:"dojo", location:"./web/dojo"},
    {name:"dijit", location:"./web/dijit"},
    {name:"dojox", location:"./web/dojox"},
    {name:"luciad", location:"./web/luciad"},
	{name:"samplecommon", location: "./web/samples/common"},
    {name:"file", location:"./web/samples/model/File/js/file"},
    {name:"c2viz", location:"./src/C2VizLima_V2/js"}
  ],
  cache:{}
}, ["c2viz"]);

