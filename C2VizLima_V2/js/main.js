var map;
var layer;
var camaraSelect;
var colorGeomet = "rgb(0,0,255)";
var ipIONE = "localhost";
var ipRIAE = "localhost";
var accessTokenAPI = "";
var incidentSelect;
var featureStoreSinPad;
var featurePuntosSinPadLayer;
var myVar;
var featureStoreIncidentesT;
var featureModelIncidentesT;
var simbolo2Array =[];
var simboloArray =[];
define([
  "luciad/view/WebGLMap",
  "dojo/aspect",
  "dojo/request",
  "dojo/dom-construct",
  "dojo/dom",
  "luciad/model/tileset/BingMapsTileSetModel",
  "luciad/reference/ReferenceProvider",
  "luciad/view/PaintRepresentation",
  "luciad/shape/ShapeFactory",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/model/tileset/FusionTileSetModel",  
  "luciad/view/LayerType",
  "luciad/model/feature/FeatureModel",
  "luciad/model/feature/Feature",
  "luciad/model/store/MemoryStore",
  "luciad/shape/ShapeType",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/feature/BasicFeaturePainter",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/controller/BasicCreateController",
  "luciad/view/controller/EditController",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/model/store/UrlStore",
  "luciad/util/ColorMap",
  "luciad/shape/format/LonLatPointFormat",
   "samplecommon/control/LayerTreeControl",
    "luciad/view/LayerGroup"
  
  
], function(WebGLMap, dAspect, request, dConstruct, dom, BingMapsTileSetModel, ReferenceProvider, PaintRepresentation, ShapeFactory, RasterTileSetLayer, FusionTileSetModel, LayerType,
			FeatureModel, Feature, MemoryStore, ShapeType, FeaturePainter, BasicFeaturePainter, FeatureLayer, BasicCreateController, EditController, GeoJsonCodec, UrlStore, ColorMap, LonLatPointFormat,
			LayerTreeControl,LayerGroup) {

	
	var mapReference = ReferenceProvider.getReference( "EPSG:4978" );
	var jsonsword;
	//conn = {}, window.WebSocket = window.WebSocket || window.MozWebSocket;
	map = new WebGLMap( "map", {reference: mapReference} );
	
	var tileSetReference = ReferenceProvider.getReference("EPSG:4326");
	var elevationParameters = {
		reference: tileSetReference,
		level0Columns: 6,
		level0Rows: 3,
		bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
		url: "http://"+ipRIAE+":8081/LuciadFusion/lts",
		coverageId: "earth_image",
		tileWidth: 255,	
		tileHeight: 255
    };
	//map.layerTree.addChild(new RasterTileSetLayer(new FusionTileSetModel(elevationParameters), { label: "Satellite imagery", layerType: LayerType.BASE }), "bottom");
	
	var elevationParameters2 = {
      reference: tileSetReference,
      level0Columns: 1,
      level0Rows: 1,
      bounds: ShapeFactory.createBounds(tileSetReference, [-85.00041654560482, 20.000833696549137, -5.000416085531469, 20.00083297011753]),
      url: "http://"+ipRIAE+":8081/LuciadFusion/lts",
      coverageId: "ElevacionSRTMCAM",
      tileWidth: 64,
      tileHeight: 64,
      dataType: FusionTileSetModel.DataType.ELEVATION,
      samplingMode: FusionTileSetModel.SamplingMode.AREA
    };
	var elavacionLayer = new RasterTileSetLayer(new FusionTileSetModel(elevationParameters2), {label: "Elevacion"});
	map.layerTree.addChild(elavacionLayer);
	
	var waitingForMetadata = false;
	var switchToBing = function(type) {
		if (waitingForMetadata) {
		  return;
		}
		if (layer) {
		  map.layerTree.removeChild(layer);
		}
		waitingForMetadata = true;
		//#snippet createModel
		request("/bingproxy/" + type, {
		  handleAs: "json"
		}).then(
			function(data) {
			  //Inspect the response
			  var resourceSet;
			  var resource = data;

			  if (Object.prototype.toString.call(data.resourceSets) === '[object Array]' &&
				  data.resourceSets.length > 0) {
				resourceSet = data.resourceSets[0];
				if (Object.prototype.toString.call(resourceSet.resources) === '[object Array]' &&
					resourceSet.resources.length > 0) {
				  resource = resourceSet.resources[0];
				}
			  }

			  resource.brandLogoUri = resource.brandLogoUri || data.brandLogoUri;
			  var model = new BingMapsTileSetModel(resource);
			  layer = new RasterTileSetLayer(model, {
				layerType: LayerType.BASE
			  });
			  map.layerTree.addChild(layer, "bottom");
			  waitingForMetadata = false;
			},
			function(error) {
			  sample.warn("BingImagery", "Bing Maps is not reachable.<br/>In some cases, clearing your browser cache can help.");
			  console.log(error);
			  waitingForMetadata = false;
			}
		);
	};
	switchToBing("aerial");
	
	function openConnection() {
		//broker_conn = "ws://192.168.30.127:85";
		broker_conn = "ws://"+ipIONE+":85";
		if (conn.readyState === undefined || conn.readyState > 1) {
			conn = new WebSocket(broker_conn);
			conn.onopen = function () {
				isWSConnected = true;
				userLogin =  $("#usuarioLogin").val();
				passLogin =  $("#passwordLogin").val();
				conn.send('{"ubicacion":[{"tipo":"7","usuario":"'+userLogin+'","password":"'+passLogin+'"}]}');
			};
			conn.onmessage = function (event) {
				lmiboxdata = event.data;
				UbicarMapafuncion();
			};
			conn.onerror = function (event) {
			isWSConnected = false;
				alert("Error de Conexin\u00F3n");
			};
			conn.onclose = function (event) {
			isWSConnected = false;
				alert("Conexin\u00F3n Cerrada");
			};
		}
	}
	
	
	
	var featureReference = ReferenceProvider.getReference ("CRS:84");
	
	
	function UbicarMapafuncion() {
		
		jsonsword = JSON.parse(lmiboxdata);
		
		switch (jsonsword.Tipo)
		{
			case 8:
				//getIncidentesT();
				break;
			default: 
				break;
		}
	};
	
	
	featureStoreIncidentesT = new MemoryStore();
	featureModelIncidentesT = new FeatureModel(featureStoreIncidentesT, {reference: featureReference});
	
	function getIncidentesT(listaincidentes)
	{
		var inciedRedrawTime = true;
		for (var i = 0; i < listaincidentes.IncidentQueryResponses.length; i++)
		{
			var id_sos = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentId + 100;
			var descripcion = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.Description;
			var fechahora = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.MissionTime;
			var fechahoraincidente = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.MissionTime;
			var incidente = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.IncidentKindName;
			var estado = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.PriorityLevel;
			var tipoestado = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.DispatchState;
			var latitud = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.Latitude;
			var longitud = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.Longtitude;
			simbolo = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.IncidentKindIconUrl;
			var direccion = listaincidentes.IncidentQueryResponses[i].IncidentDetailsSection.IncidentDetailsData.Street;
			
			/* if(simbolo == "https://img.nowforce.com/lb/2385903.jpg")
				 simbolo = "uno";
			 if(simbolo == "https://img.nowforce.com/lb/2385968.jpg")
				 simbolo = "dos";
			if(simbolo == "https://img.nowforce.com/lb/606596.jpg")
				simbolo = "tres";
			if(simbolo == "https://img.nowforce.com/lb/2385871.jpg")
				simbolo = "cuatro";
			if(simbolo == "https://img.nowforce.com/lb/2385922.jpg")
				simbolo = "cinco";
			if(simbolo == "https://img.nowforce.com/lb/4810.jpg")
				simbolo = "cinco";*/
			
			var item5 = document.createElement('div');
			var img5 = document.createElement('img');
			img5.src = simbolo;
			img5.style.width = '13px';
			img5.style.height = '13px';
			item5.appendChild(img5);
			
			var point_SOS_consulta = ShapeFactory.createPoint(featureReference, [longitud, latitud, 0]);
			
			
			var itemNew = {event_id: (id_sos), content: item5, start: new Date(fechahora), className:'yellow'}
			dataTimeline.push(itemNew)
			
			
			
			var featureTemp_actas_consulta = featureModelIncidentesT.get(id_sos);
			var actas_consulta = new Feature(point_SOS_consulta, {
				id_sos: id_sos,
				descripcion: descripcion,
				fechahora: fechahora,
				fechahoraincidente: fechahoraincidente,
				incidente: incidente,
				estado: estado,
				tipoestado: tipoestado,
				latitud: latitud,
				longitud: longitud,
				simbolo: simbolo
			},id_sos);
			
			if(featureTemp_actas_consulta)
			{
				if(featureTemp_actas_consulta.shape[0] != actas_consulta.shape[0] || featureTemp_actas_consulta.shape[1] != actas_consulta.shape[1])
				{
					featureModelIncidentesT.put(actas_consulta);
				}
				inciedRedrawTime = false;
			}
			else
			{
				featureModelIncidentesT.add(actas_consulta);
				//items._addItem(itemNew);
			
			}
		}
		if(inciedRedrawTime){
		 //timeline.setItems(items);
		 timeline.draw(dataTimeline);
		}
			
	}
	
	function MensajeBalloonIncidentesT() {
	  return function(o) {
		  featInfoEnvi = o;
		  return "<div id='objetoswordpane' class='formpanel' style='padding:10px; overflow-y: scroll; height: 250px; width: 100%; margin-bottom :5px;'>"+
				"<table  class='table' width='250'><tbody>"+
				
				"<tr>"+
				"<td>"+
				"<label>Descripción: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["descripcion"] + "</label>"+
				"</td>"+
				"</tr>"+
				"<tr>"+
				"<td>"+
				"<label>Fecha: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["fechahora"] + "</label>"+
				"</td>"+
				"</tr>"+
				"<tr>"+
				"<td>"+
				"<label>Incidente: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["incidente"] + "</label>"+
				"</td>"+
				"</tr>"+
				"<tr>"+
				"<td>"+
				"<label>Estado: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["tipoestado"] + "</label>"+
				"</td>"+
				"</tr>"+

				
				
				"</tbody></table>"+
				"</div>";
	  };
	};
	
	var actasPainterConsulta = new FeaturePainter();
	actasPainterConsulta.paintBody = function (geocanvas, feature, shape, layer, map, paintState)
	{
		geocanvas.drawIcon( shape, {width: 20, height: 20, url: feature.properties["simbolo"], offsetX: 0,offsetY: 0});
		if (paintState.selected ) {
			geocanvas.drawIcon(shape, {width: 10, height: 10, url: "img/visto.png"} );
		}
	}
	
	var IncidentesLayer = new FeatureLayer(featureModelIncidentesT, {
		selectable: true,
		editable: true,
		label: "Incidentes Consultados",
		painter: actasPainterConsulta
	});
	
	map.layerTree.addChild(IncidentesLayer, "top");
	IncidentesLayer.balloonContentProvider = MensajeBalloonIncidentesT();
	
	
	
	
	
	
	function createPuntoModel() {
			var PuntosUIOStore = new UrlStore({
			  target: "data/lima.geojson",
			  codec: new GeoJsonCodec()
			});
			var reference = ReferenceProvider.getReference("CRS:84");
			var modeloSal1 = new FeatureModel(PuntosUIOStore, {
			  reference: reference
			});
			
			return modeloSal1;
		  }
		  
		var PuntosUIOPainter = new FeaturePainter();
		var featurePuntosUIOLayer;
        PuntosUIOPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
        {
			switch(feature.properties["tipo"])
			{
				case "Artesano": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/artesania.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Iglesia": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/iglesia.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Parada_Bus": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/busP.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_advertising": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_animal_boarding": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/animal.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_arts_centre": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_atm": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/atm.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bank": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Banco.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bar": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/bar.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bbq": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Restaurant.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bench": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bicycle_parking": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Bicicleta.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bicycle_rental": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Bicicleta.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bicycle_repair_station": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Bicicleta.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bureau_de_change": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_bus_station": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/busP.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_cafe": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Cafe.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_cafe;ice_cream": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Cafe.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_car_rental": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/RentaCar.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_car_wash": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/CarWash.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_charging_station": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_childcare": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_cinema": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_clinic": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Hospital.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_college": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_community_centre": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_computer": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_coworking_space": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_dancing_school": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_dentist": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_doctors": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_dojo": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_drinking_water": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_driving_school": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_embassy": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_fast_food": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Rest.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_fire_station": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/bomberos.PNG" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_food_court": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Rest.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_fountain": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_fuel": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_hospital": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_ice_cream": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Rest.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_internet": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Cyber.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_internet_cafe": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Cyber.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_kindergarten": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/schoolK.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_language_school": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_library": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_marketplace": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_money_transfer": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_music_school": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/schoolM.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_nightclub": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_parking": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Parking.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_parking_entrance": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_parking_space": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_pharmacy": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/CENTRO DE SALUD TIPO B.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_place_of_worship": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Templo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_police": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Policia.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_post_box": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_post_office": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_pub": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/bar.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_public_building": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_recycling": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_restaurant": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Restaurant.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_school": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/school.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_shelter": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_shower": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_showers": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_social_centre": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_social_facility": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_spa": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_studio": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_taxi": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_telephone": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Telefono.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_theatre": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_toilets": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_townhall": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_training": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_university": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/school.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_veterinary": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/animal.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_waste_basket": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_waste_disposal": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Sitio_water_point": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_alpine_hut": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_artwork": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_attraction": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_camp_site": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_chalet": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_gallery": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/GaleriaArte.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_guest_house": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_hostel": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_hotel": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_information": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_motel": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_museum": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_picnic_site": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_theme_park": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "Turismo_viewpoint": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/turismo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "elevacion": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_dance": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_fitness_centre": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_park": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_picnic_table": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_pitch": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_playground": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_sauna": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_slipway": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_sports_centre": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/cancha.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_swimming_pool": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_track": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "entret_water_park": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "info": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "tiendas": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/shop.png" , offsetX: 0,offsetY: 0}); 
					break;
				default:
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/iconos_custom/Tachuela.png" , offsetX: 0,offsetY: 0});
			}
			if (paintState.selected ) {
				geocanvas.drawIcon(shape, {width: 10, height: 10, url: "../../iconos/visto.png"} );
			}
		
		
        	
        }
		  
		 featurePuntosUIOLayer = new FeatureLayer(createPuntoModel(), {
        	  selectable: true,
        	  editable: true,
        	  label: "Quito",
			  painter: PuntosUIOPainter,
			  minScale: 0.5e-4			  
        });
		
		function MensajeBalloonPuntosUIO(){
          return function(o) {
            return "<div id='objetoswordpane' class='formpanel' style='padding:10px'>"+
				"<table  class='table' width='250'>"+
				
				"<tbody><tr>"+
				"<td>"+
				"<label>Nombre: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["name"] + "</label>"+
				"</td>"+
				"</tr></tbody>"+	
				
				"</table> "+
				"</div>";
          };

		};
		featurePuntosUIOLayer.balloonContentProvider = MensajeBalloonPuntosUIO();
		
		map.layerTree.addChild(featurePuntosUIOLayer);
	  
		featureStoreSinPad = new MemoryStore();
		featureModelSinPad = new FeatureModel(featureStoreSinPad, {reference: featureReference});
		  
		var PuntosSinPadPainter = new FeaturePainter();
        PuntosSinPadPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
        {
			
			switch(feature.properties["Fenomeno"])
			{
				case "DERRUMBE": 
				case "DESLIZAMIENTO":
				case "HUAYCO": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/derrumbe.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "DESCENSO DE TEMPERATURA": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/temperaturaB.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "EJERCICIO": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/warningv.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "INCENDIO URBANO": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/incendios.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "INUNDACION": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/inundaciones.png" , offsetX: 0,offsetY: 0}); 
					break; 
				case "PRECIPITACIONES - GRANIZO": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/hielo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "PRECIPITACIONES - LLUVIA": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/lluvia.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "SISMOS": 
				case "OTROS DE GEODINAMICA EXTERNA":
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/terremoto.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "TORMENTA ELECTRICA (TEMPESTAD ELECTRICA)": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/rayo.png" , offsetX: 0,offsetY: 0}); 
					break;
				case "VIENTOS FUERTES": 
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/huracanes.png" , offsetX: 0,offsetY: 0}); 
					break;
				default:
					geocanvas.drawIcon( shape, {width: 20, height: 20, url: "img/warninga.png" , offsetX: 0,offsetY: 0});
			}
			if (paintState.selected ) {
				geocanvas.drawIcon(shape, {width: 10, height: 10, url: "img/visto.png"} );
			}        	
        }
		  
		 featurePuntosSinPadLayer = new FeatureLayer(featureModelSinPad, {
        	  selectable: true,
        	  editable: true,
        	  label: "SINPAD",
			  painter: PuntosSinPadPainter			  
        });
		
		function MensajeBalloonPuntosSinPad(){
          return function(o) {
			  incidentSelect = o;
            return "<div id='objetoswordpane' class='formpanel' style='padding:10px'>"+
				"<table  class='table' width='250'>"+
				
				"<tbody>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>Emergencia: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Emergencia"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>C\u00F3digo: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Codigo"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>Fen\u00F3meno: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Fenomeno"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>Departamento: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Departamento"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>Provincia: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Provincia"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>Distrito: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Distrito"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"<tr>"+
				"<td>"+
				"<label>Fecha: </label>"+
				"</td>"+
				"<td >"+
				"<label>" + o.properties["Fecha"] + "</label>"+
				"</td>"+
				"</tr>"+
				
				
				"</tbody>"+	
				
				"</table><button onclick='ReporteIncidentes()'>Reporte</button> "+
				"</div>";
          };

		};
		featurePuntosSinPadLayer.balloonContentProvider = MensajeBalloonPuntosSinPad();
		
		map.layerTree.addChild(featurePuntosSinPadLayer);
	
	
	
	
	function createBuildingsModelQuito() {
			var modelRef = ReferenceProvider.getHeightAboveTerrainReference("EPSG:4326");

			var delegateCodec = new GeoJsonCodec();
			// overwrite the reference with an above ground reference
			var decodeGeometryOriginal = delegateCodec.decodeGeometryObject;
			delegateCodec.decodeGeometryObject = function(geometry, reference) {
			  return decodeGeometryOriginal.call(this, geometry, modelRef);
			};
			// create a codec that converts the shapes to extruded shapes
			var extrudedCodec = {
			  _delegate: delegateCodec,
			  decode: function(object) {
				var cursor = this._delegate.decode(object);
				return {
				  hasNext: function() {
					return cursor.hasNext();
				  },
				  next: function() {
					var baseFeature = cursor.next();
					var height = Math.floor(Math.random() * (20 - 10)) + 10;
					return new Feature(
						ShapeFactory.createExtrudedShape(baseFeature.shape.reference,
							baseFeature.shape,
							0.0,
							height
						),
						{
						  HEIGHT: height
						},
						baseFeature.id
					);
				  }
				};
			  }
			};

			var store = new UrlStore({target: "data/PredioLima.geojson", codec: extrudedCodec});
			return new FeatureModel(store, {
			  reference: modelRef
			});
		}
		
		
		var createPredioUQuitoModel= createBuildingsModelQuito();
		
		var featurePredioUQuitoPainter = new FeaturePainter();
		featurePredioUQuitoPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
        {
			if(feature.properties["HEIGHT"] > 9 && feature.properties["HEIGHT"] <= 13)
			{
				geocanvas.drawShape(shape, {
					stroke: {
						color: "rgb(0,0,0)",
						width: 1
					},
					fill: { color: "rgba(0,204,102,0.5)"}
				});
			}
			else
			{
				if(feature.properties["HEIGHT"] > 13 && feature.properties["HEIGHT"] <= 16)
				{
					geocanvas.drawShape(shape, {
						stroke: {
							color: "rgb(0,0,0)",
							width: 1
						},
						fill: { color: "rgba(0,0,255,0.5)"}
					});
				}
				else
				{
					geocanvas.drawShape(shape, {
						stroke: {
							color: "rgb(0,0,0)",
							width: 1
						},
						fill: { color: "rgba(228, 208, 0,0.5)"}
					});
				}
			}
		}
     
		var featurePredioUQuitoLayer = new FeatureLayer(createPredioUQuitoModel, {
        	  selectable: true,
        	  editable: true,
        	  label: "Predio Urbano Quito",
			  painter: featurePredioUQuitoPainter
        });
		
		map.layerTree.addChild(featurePredioUQuitoLayer);
	
	
	
		
	$("#ocultarTimeLine").click(function(){
		 var content = document.getElementById("timeline");
        var mapacompleto= document.getElementById("map");

        content.classList.toggle("hide");
        if (mapacompleto.classList.contains('mapa')) {

            mapacompleto.classList.remove('mapa');
            mapacompleto.classList.add('maximizar');
            mapacompleto.style.height="100%";
            map.resize();

        } else {
            mapacompleto.classList.remove('maximizar');
            mapacompleto.classList.add('mapa');
            content.classList.remove("hide");
            mapacompleto.style.height="70%";

        }
	});
		
	$("#BingCalles").click(function(){
		switchToBing("road");
	});
	
	
	
	$("#BingSatelital").click(function(){
		switchToBing("aerial");
	});
			
	var drawingPainter = new FeaturePainter();
	drawingPainter.paintBody = function (geocanvas, feature, shape, layer, map, paintState )
	{
		geocanvas.drawShape(shape, { stroke: { color: colorGeomet, width: 3 } });
		if (paintState.selected ) {
			geocanvas.drawShape(shape, { stroke: { color: "rgb(255,255,255)", width: 3 } });
		}
	}
	
	var geometryStoreDrawing = new MemoryStore();
        var geometryModelDrawing = new FeatureModel(geometryStoreDrawing, {reference: featureReference});
        var geometryLayer = new FeatureLayer (geometryModelDrawing,
        {
        	selectable: true,
            editable: true,
            label: "Geometria",
			painter: drawingPainter
        });
	map.layerTree.addChild(geometryLayer);
		
	map.mapNavigator.fit(ShapeFactory.createBounds(featureReference,
					[-76.828880, 10, -11.553247, 10]), false);
		
	var mapLayer = [];
	var idA = [];
  
	
  
    $('#hospitalesantioquiali').click(function(){
	   map.mapNavigator.fit(featurePuntosAntioquiaLayer.bounds, false);
	});
	
	$('#nuevoUsuarioMod').click(function(){
		$('#myModalNuevoUsuario').modal('show');
	});
	
	$('#agregarNuevoUsuario').click(function(){
		$('#myModalTablaFeature').modal('show');
	});
	
	function buscarLayer(name){
		var capa;
		switch(name) {
			case "Bing maps":
				capa = layer;
				break;
			case "Lima":
				capa = featurePuntosUIOLayer;
				break;
			case "Incidentes NowForce":
				capa = IncidentesLayer;
				break;
			case "Incidentes SINPAD":
				capa = featurePuntosSinPadLayer;
				break;
			case "Catastro 3D":
				capa = featurePredioUQuitoLayer;
				break;
			default:
				break;
		}
		return capa;
	}
	
	$(this).keypress(function(event)
	{
		var keycode = (event.keyCode ? event.keyCode : event.which);
		 //Edit Geometry (e)
		 if ( keycode == "69" || keycode == "101") {
			   if (map.selectedObjects[0]) {
				 map.controller = new EditController(map.selectedObjects[0].layer, map.selectedObjects[0].selected[0]);
			   }
		 }
		 //Delete Geometry (del or backspace)
		 if ( keycode == "46" || keycode == "8") {
			 if ( map.selectedObjects[0] && geometryLayer === map.selectedObjects[0].layer ) {
			  geometryModelDrawing.remove( map.selectedObjects[0].selected[0].id );
			 }
		}
	});
	
	$("#envioNoticiaNew").click(function(){
			
	
	
	});
	
	$("#buscarPlacaN").click(function(){
		
	});
	
	
	$("#buscarPorDirecc").click(function(){
		
	});
	
	
	$("#buscarCamarasN").click(function(){
		
	});
	
	
	$("#buscarParadasN").click(function(){
		
	});
	$("#login").click(function(){
		$('#myModalLogin').modal('show');
	});
			
	$("#mensajeNuevo").click(function(){
		$('#myModal').modal('show');
	});
	$("#busqueda").click(function(){
		$('#myModalBusqueda').modal('show');
	});
		
	$("#usuarios").click(function(){
		$('#myModalTablaFeature').modal('show');
	});
	
	$(document).ready(function () {
		conn = {}, window.WebSocket = window.WebSocket || window.MozWebSocket;
		$("#connectsword").click(function(){
			openConnection();
		});
		
		
		$('[data-toggle="popover"]').popover({ html : true }).on('shown.bs.popover', function() {
			$("#graficarArea").click(function(){
				GraficarTipo(0);
			});	
			$("#graficarCirculo").click(function(){
				GraficarTipo(1);
			});
			$("#graficarCirculo2").click(function(){
				GraficarTipo(2);
			});
			$("#graficarOvalo").click(function(){
				GraficarTipo(3);
			});
			var colorInput = document.getElementById("colorGraf");
			colorInput.addEventListener("input", function() {
				colorGeomet = colorInput.value;
			}, false); 
		});
		
		function GraficarTipo(tipoG)
		{
			var tipoAgraf = ShapeType.POLYGON;
			if(tipoG == 0)
			{
				tipoAgraf = ShapeType.POLYGON;
			}
			if(tipoG == 1)
			{
				tipoAgraf = ShapeType.CIRCLE_BY_3_POINTS;
			}
			if(tipoG == 2)
			{
				tipoAgraf = ShapeType.CIRCLE_BY_CENTER_POINT;
			}
			if(tipoG == 3)
			{
				tipoAgraf = ShapeType.ELLIPSE;
			}
			var value = geometryModelDrawing.query();
			while(value.hasNext())
			{
				geometryModelDrawing.remove(value.next().id)
			}
			var polyController = new BasicCreateController(tipoAgraf);
			polyController.setPointCount(3, -1);
			map.controller = polyController;
			
		}
		
		$.ajax({url: "data/ParametrosC2Viz.json", success: function(resultParm){
			 ipRIAE = resultParm.ipRIA;
			 ipIONE = resultParm.ipION;
		}});
		
		$.ajax({url: "data/sinpad.geojson", success: function(resultObt){
			var codecGson = new GeoJsonCodec({mode3D: false});
			var cursorFeaTe = codecGson.decode({content: resultObt});
			var redrawTime = true;
			while(cursorFeaTe.hasNext())
			{
				var featTemp = cursorFeaTe.next();
				var featTempExist = featureStoreSinPad.get(featTemp.id);
				//featTemp.properties.Name;
				
				if(featTempExist)
				{
					redrawTime = false;
				}
				else
				{
					featureModelSinPad.add(featTemp);
					
					var item6 = document.createElement('div');
					var img6 = document.createElement('img');
					
					var simbolo2;
					switch(featTemp.properties["Fenomeno"])
					{
						case "DERRUMBE": 
						case "DESLIZAMIENTO":
						case "HUAYCO": 
							img6.src = "img/derrumbe.png"; 
							simbolo2 = "huayco"; 
							break;
						case "DESCENSO DE TEMPERATURA": 
							img6.src = "img/temperaturaB.png"; 
							simbolo2 = "tempDes"; 
							break;
						case "EJERCICIO": 
							img6.src = "img/warningv.png"; 
							simbolo2 = "ejercicio"; 
							break;
						case "INCENDIO URBANO": 
							img6.src = "img/incendios.png"; 
							simbolo2 = "incendio";  
							break;
						case "INUNDACION": 
							img6.src = "img/inundaciones.png"; 
							simbolo2 = "inundacion"; 
							break; 
						case "PRECIPITACIONES - GRANIZO": 
							img6.src = "img/hielo.png"; 
							simbolo2 = "granizo"; 
							break;
						case "PRECIPITACIONES - LLUVIA": 
							img6.src = "img/lluvia.png"; 
							simbolo2 = "lluvia";  
							break;
						case "SISMOS": 
						case "OTROS DE GEODINAMICA EXTERNA":
							img6.src = "img/terremoto.png";
							simbolo2 = "otro";
							break;
						case "TORMENTA ELECTRICA (TEMPESTAD ELECTRICA)": 
							img6.src = "img/rayo.png"; 
							simbolo2 =  "tormenta"; 
							break;
						case "VIENTOS FUERTES": 
							img6.src = "img/huracanes.png"; 
							simbolo2 = "viento"; 

							break;
						default:
							img6.src = "img/warninga.png";
							simbolo2 = "yellow";
					}
					img6.style.width = '13px';
					img6.style.height = '13px';
					item6.appendChild(img6);
					simbolo2Array.push(simbolo2);
					//19-02-2018 04:30:00 a.m.
					console.log(""+simbolo2);
					var fechaDescCamb = featTemp.properties.Fecha.split(" ")[0];
					var horaDescCamb = featTemp.properties.Fecha.split(" ")[1];
					var ampmDescCamb = featTemp.properties.Fecha.split(" ")[2];
					
					var fechaInciTemp;
					
					if(ampmDescCamb.includes("a"))
					{
						fechaInciTemp = new Date(fechaDescCamb.split("-")[2],fechaDescCamb.split("-")[1] - 1,fechaDescCamb.split("-")[0],horaDescCamb.split(":")[0],horaDescCamb.split(":")[1],0,0);
					}
					else
					{
						fechaInciTemp = new Date(fechaDescCamb.split("-")[2],fechaDescCamb.split("-")[1] - 1,fechaDescCamb.split("-")[0],parseInt(horaDescCamb.split(":")[0])+12,horaDescCamb.split(":")[1],0,0);
					}
					var itemNew = {event_id: featTemp.id, content: item6, start: fechaInciTemp, className:'yellow'};
					
					//items._addItem(itemNew);
					 dataTimeline.push(itemNew)
					
				}
			}
			if(redrawTime){
				timeline.draw(dataTimeline)
			}
				//timeline.setItems(items);
		}});
		
		function myTimer() {
			$.ajax({url: "http://lb.nowforce.com/api/en-us/incident/Query3/json/3110/3110?ShowIncidentDetails=true&ShowActiveUsersInIncident=true", 
			type: 'get',
			headers: {
				"Authorization": "acaf78cc-a55c-4ed9-b6c5-42ac6306ebb4"
			},
			success: function(resultObt){
				getIncidentesT(resultObt);
			}});
		}
		function myStopFunction() {
			clearInterval(myVar);
		}
		
		myVar = setInterval(function(){ myTimer() }, 30000);
		
	});
 
 (new LayerTreeControl(map, {domId: "controlcontainer"}));

 
 });

	
function cambiarFocus(valorCambiado)
{
	
	$.ajax
	({
		  type: "GET",
		  dataType: 'json',
		  url: "http://169.254.200.211/axis-cgi/opticssetup.cgi?afocus="+valorCambiado/297,
		  beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", "Basic " + btoa("root:12345678"));
		  }
});
}

function abrirModal()
{
	document.getElementById('camaraZoom').src = "http://"+camaraSelect.properties["usuario"]+":"+camaraSelect.properties["passwordC"]+"@"+camaraSelect.properties["ip"]+"/axis-cgi/mjpg/video.cgi?fps=12&resolution=768x576";
	
	$('#myModalCamraa').modal('show');
}

function ReporteIncidentes()
{
	document.getElementById('reporteIncid').src = "http://sinpad.indeci.gob.pe/sinpad/emergencias/Evaluacion/Reporte/rpt_eme_situacion_emergencia.asp?EmergCode=000" + incidentSelect.properties["Codigo"];
	$('#myModalReporte').modal('show');
}


function ZoomIncedenteSelec(itemPZ)
{
	var featureTempZoom = featureStoreSinPad.get(itemPZ);
	var boundsInc = featureTempZoom.shape.bounds;
	boundsInc.setTo2D(boundsInc.x-0.002,0.004,boundsInc.y-0.002,0.004);
	map.mapNavigator.fit(boundsInc);
}

function ZoomIncedenteSelecNF(itemPZ)
{
	var featureTempZoom = featureStoreIncidentesT.get(itemPZ);
	var boundsInc = featureTempZoom.shape.bounds;
	boundsInc.setTo2D(boundsInc.x-0.002,0.004,boundsInc.y-0.002,0.004);
	map.mapNavigator.fit(boundsInc);
}