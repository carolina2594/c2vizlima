window.__lcdBasePath = '../../';
window.__lcdMobileTemplate = false;
function resetMapSize() {
    var ui = $('#after');
var height = window.innerHeight - ui.outerHeight();
$('#map').height(height);
}

$(function() {



function requestFullscreen(elem) {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.msRequestFullscreen) {
    elem.msRequestFullscreen();
  } else if (elem.mozRequestFullScreen) {
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) {
    elem.webkitRequestFullscreen();
  }
}
function cancelFullscreen() {
  if (document.cancelFullScreen) {
    document.cancelFullScreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitCancelFullScreen) {
    document.webkitCancelFullScreen();
  } else if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
}
function isFullscreen() {
  return !!(document.fullscreenElement || // alternative standard method
      document.mozFullScreenElement
  || document.webkitFullscreenElement
  || document.msFullscreenElement); // current working methods
}
$("#fullscreen").click(function() {
  var elem = document.getElementById("map");
  if (!isFullscreen()) {
    requestFullscreen(elem);
  } else {
    cancelFullscreen();
  }
});
function toggleFullscreenButtonIcon() {
  $("#fullscreen-button").toggleClass("glyphicon-fullscreen", !isFullscreen());
  $("#fullscreen-button").toggleClass("glyphicon-resize-small", isFullscreen());
}
document.addEventListener("fullscreenchange", toggleFullscreenButtonIcon, false);
document.addEventListener("mozfullscreenchange", toggleFullscreenButtonIcon, false);
document.addEventListener("webkitfullscreenchange", toggleFullscreenButtonIcon, false);
document.addEventListener("MSFullscreenChange", toggleFullscreenButtonIcon, false);
  });